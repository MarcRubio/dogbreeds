package com.cuvva.dogbreeds.net.allbreeds

import com.cuvva.dogbreeds.entity.Breed

class AllBreedsRepositoryImplFake : AllBreedsRepository {
    override suspend fun retrieveAllDogBreeds(): List<Breed> {
        return allBreeds
    }

    companion object {
        val allBreeds = listOf(
            Breed("BREED01", "SUB_BREED01"),
            Breed("BREED02", "SUB_BREED02"),
            Breed("BREED03", "SUB_BREED03"),
            Breed("BREED04", "SUB_BREED04"),
            Breed("BREED05", "SUB_BREED05"),
            Breed("BREED06", "SUB_BREED06"),
            Breed("BREED07", "SUB_BREED07"),
            Breed("BREED08", "SUB_BREED08"),
            Breed("BREED09", "SUB_BREED09"),
            Breed("BREED10", "SUB_BREED10")
        )
    }
}
