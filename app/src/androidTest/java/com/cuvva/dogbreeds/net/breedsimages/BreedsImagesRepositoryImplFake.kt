package com.cuvva.dogbreeds.net.breedsimages

import com.cuvva.dogbreeds.entity.BreedsImages

class BreedsImagesRepositoryImplFake : BreedsImagesRepository {
    override suspend fun retrieveAllBreedsImages(breed: String): List<BreedsImages> {
        return breedsImages
    }

    override suspend fun retrieveAllSubBreedsImages(
        breed: String,
        subBreed: String
    ): List<BreedsImages> {
        return breedsImages
    }

    companion object {
        val breedsImages = listOf(
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_1003.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_1007.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_1023.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_10263.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_10715.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_10822.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_10832.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_10982.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_11006.jpg"),
            BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_11172.jpg")
        )
    }
}
