package com.cuvva.dogbreeds.ui.allbreeds

import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.cuvva.dogbreeds.R
import com.cuvva.dogbreeds.net.allbreeds.AllBreedsRepositoryImplFake
import com.cuvva.dogbreeds.ui.DogBreedsActivity
import com.schibsted.spain.barista.assertion.BaristaListAssertions.assertListItemCount
import com.schibsted.spain.barista.assertion.BaristaListAssertions.assertListNotEmpty
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.schibsted.spain.barista.interaction.BaristaListInteractions.clickListItem
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class AllBreedsFragmentTest {

    @get:Rule
    val activityScenarioRule = activityScenarioRule<DogBreedsActivity>()

    @Test
    fun given_app_launched_When_data_is_fetched_Then_list_of_breeds_is_populated() {
        assertDisplayed(R.id.allBreedsRecyclerView)
        assertListNotEmpty(R.id.allBreedsRecyclerView)
        assertListItemCount(R.id.allBreedsRecyclerView, 10)
        assertDisplayed(AllBreedsRepositoryImplFake.allBreeds.first().breed)
    }

    @Test
    fun given_app_launched_When_data_is_fetched_and_item_clicked_Then_navigate_to_list_of_images() {
        clickListItem(R.id.allBreedsRecyclerView, 0)
        assertListNotEmpty(R.id.breedsImagesRecyclerView)
        assertListItemCount(R.id.breedsImagesRecyclerView, 10)
        assertDisplayed(R.id.itemBreedImageView)
    }
}
