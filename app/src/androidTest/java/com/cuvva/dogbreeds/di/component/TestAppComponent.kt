package com.cuvva.dogbreeds.di.component

import com.cuvva.dogbreeds.di.module.BaseViewModule
import com.cuvva.dogbreeds.di.module.DogBreedsModuleTest
import com.cuvva.dogbreeds.di.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [BaseViewModule::class, NetworkModule::class, DogBreedsModuleTest::class])
interface TestAppComponent : AppComponent
