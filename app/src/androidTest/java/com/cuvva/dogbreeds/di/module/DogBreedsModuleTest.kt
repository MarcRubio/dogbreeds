package com.cuvva.dogbreeds.di.module

import com.cuvva.dogbreeds.net.allbreeds.AllBreedsRepository
import com.cuvva.dogbreeds.net.allbreeds.AllBreedsRepositoryImplFake
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesRepository
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesRepositoryImplFake
import dagger.Module
import dagger.Provides

@Module
object DogBreedsModuleTest {

    @Provides
    @JvmStatic
    fun provideAllBreedsRepository(): AllBreedsRepository =
        AllBreedsRepositoryImplFake()

    @Provides
    @JvmStatic
    fun provideBreedsImagesRepository(): BreedsImagesRepository =
        BreedsImagesRepositoryImplFake()
}
