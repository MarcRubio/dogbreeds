package com.cuvva.dogbreeds

import com.cuvva.dogbreeds.di.component.AppComponent
import com.cuvva.dogbreeds.di.component.DaggerTestAppComponent

class DogBreedsTestApp : DogBreedsApp() {

    override fun initializeComponent(): AppComponent {
        return DaggerTestAppComponent.create()
    }
}
