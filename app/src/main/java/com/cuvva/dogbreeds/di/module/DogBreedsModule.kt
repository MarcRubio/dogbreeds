package com.cuvva.dogbreeds.di.module

import com.cuvva.dogbreeds.entity.Breed
import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.mapper.AllBreedsDataMapper
import com.cuvva.dogbreeds.mapper.BreedsImagesDataMapper
import com.cuvva.dogbreeds.mapper.DataMapper
import com.cuvva.dogbreeds.net.DogBreedsApi
import com.cuvva.dogbreeds.net.allbreeds.AllBreedsApi
import com.cuvva.dogbreeds.net.allbreeds.AllBreedsRepository
import com.cuvva.dogbreeds.net.allbreeds.AllBreedsRepositoryImpl
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesApi
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesRepository
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
object DogBreedsModule {

    @Provides
    @JvmStatic
    fun provideAllBreedsMapper(): DataMapper<AllBreedsApi, List<Breed>> =
        AllBreedsDataMapper()

    @Provides
    @JvmStatic
    fun provideBreedsImagesMapper(): DataMapper<BreedsImagesApi, List<BreedsImages>> =
        BreedsImagesDataMapper()

    @Provides
    @JvmStatic
    fun provideAllBreedsRepository(
        dogBreedsApi: DogBreedsApi,
        allBreedsDataMapper: DataMapper<AllBreedsApi, List<Breed>>
    ): AllBreedsRepository =
        AllBreedsRepositoryImpl(dogBreedsApi, allBreedsDataMapper)

    @Provides
    @JvmStatic
    fun provideBreedsImagesRepository(
        dogBreedsApi: DogBreedsApi,
        breedsImagesDataMapper: DataMapper<BreedsImagesApi, List<BreedsImages>>
    ): BreedsImagesRepository =
        BreedsImagesRepositoryImpl(dogBreedsApi, breedsImagesDataMapper)
}
