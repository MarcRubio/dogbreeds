package com.cuvva.dogbreeds.di.component

import android.content.Context
import com.cuvva.dogbreeds.ui.allbreeds.AllBreedsFragment
import com.cuvva.dogbreeds.ui.breedsimages.BreedsImagesFragment
import com.cuvva.dogbreeds.di.module.BaseViewModule
import com.cuvva.dogbreeds.di.module.DogBreedsModule
import com.cuvva.dogbreeds.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [BaseViewModule::class, NetworkModule::class, DogBreedsModule::class])
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun inject(fragment: AllBreedsFragment)
    fun inject(fragment: BreedsImagesFragment)
}
