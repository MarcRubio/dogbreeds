package com.cuvva.dogbreeds.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cuvva.dogbreeds.di.ViewModelFactory
import com.cuvva.dogbreeds.di.ViewModelKey
import com.cuvva.dogbreeds.ui.allbreeds.AllBreedsViewModel
import com.cuvva.dogbreeds.ui.breedsimages.BreedsImagesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class BaseViewModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(AllBreedsViewModel::class)
    internal abstract fun allBreedsViewModel(viewModel: AllBreedsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BreedsImagesViewModel::class)
    internal abstract fun breedsImagesViewModel(viewModel: BreedsImagesViewModel): ViewModel
}
