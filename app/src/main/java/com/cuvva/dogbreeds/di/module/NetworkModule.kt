package com.cuvva.dogbreeds.di.module

import com.cuvva.dogbreeds.BuildConfig
import com.cuvva.dogbreeds.net.DogBreedsApi
import com.cuvva.dogbreeds.util.BASE_URL
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
object NetworkModule {

    @Provides
    @JvmStatic
    internal fun provideLoggingInterceptor(): HttpLoggingInterceptor? =
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        } else null

    @Provides
    @JvmStatic
    internal fun provideOkHttpClientBuilder(
        loggingInterceptor: HttpLoggingInterceptor?
    ): OkHttpClient.Builder =
        OkHttpClient.Builder()
            .apply {
                loggingInterceptor?.also {
                    addInterceptor(it)
                }
            }

    @Provides
    @JvmStatic
    internal fun providesOkHttpClient(
        builder: OkHttpClient.Builder
    ): OkHttpClient =
        builder.build()

    @Provides
    @Singleton
    @JvmStatic
    internal fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

    @Provides
    @JvmStatic
    internal fun provideDogBreedsApi(retrofit: Retrofit): DogBreedsApi =
        retrofit.create(DogBreedsApi::class.java)
}
