package com.cuvva.dogbreeds

import android.app.Application
import com.cuvva.dogbreeds.di.component.AppComponent
import com.cuvva.dogbreeds.di.component.DaggerAppComponent
import timber.log.Timber

open class DogBreedsApp : Application() {

    val appComponent: AppComponent by lazy {
        initializeComponent()
    }

    open fun initializeComponent(): AppComponent {
        return DaggerAppComponent.factory().create(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}
