package com.cuvva.dogbreeds.entity

data class BreedsImages(val breedImageUrl: String)
