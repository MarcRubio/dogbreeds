package com.cuvva.dogbreeds.entity

data class Breed(val breed: String, val subBreed: String?)
