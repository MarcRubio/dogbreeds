package com.cuvva.dogbreeds.mapper

import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesApi

class BreedsImagesDataMapper : DataMapper<BreedsImagesApi, List<BreedsImages>> {
    override fun map(source: BreedsImagesApi): List<BreedsImages> {
        return source.message.map { BreedsImages(it) }
    }
}
