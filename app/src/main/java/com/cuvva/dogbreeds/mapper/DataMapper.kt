package com.cuvva.dogbreeds.mapper

interface DataMapper<S, R> {
    fun map(source: S): R
}
