package com.cuvva.dogbreeds.mapper

import com.cuvva.dogbreeds.net.allbreeds.AllBreedsApi
import com.cuvva.dogbreeds.entity.Breed

class AllBreedsDataMapper : DataMapper<AllBreedsApi, List<Breed>> {

    override fun map(source: AllBreedsApi): List<Breed> {
        val breeds = mutableListOf<Breed>()

        source.breeds.forEach { (key, value) ->
            if (value.isNotEmpty()) {
                val subBreeds = value.map {
                    Breed(key, it)
                }
                breeds.addAll(subBreeds)
            } else {
                breeds.add(Breed(key, null))
            }
        }

        return breeds
    }
}
