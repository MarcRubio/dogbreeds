package com.cuvva.dogbreeds.net.allbreeds

import com.squareup.moshi.Json

data class AllBreedsApi(
    @field:Json(name = "message") val breeds: Map<String, List<String>>,
    val status: String
)
