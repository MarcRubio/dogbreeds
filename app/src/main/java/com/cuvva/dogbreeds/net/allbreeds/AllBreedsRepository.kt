package com.cuvva.dogbreeds.net.allbreeds

import com.cuvva.dogbreeds.entity.Breed
import com.cuvva.dogbreeds.net.Repository

interface AllBreedsRepository : Repository {
    suspend fun retrieveAllDogBreeds(): List<Breed>
}
