package com.cuvva.dogbreeds.net.allbreeds

import com.cuvva.dogbreeds.entity.Breed
import com.cuvva.dogbreeds.mapper.DataMapper
import com.cuvva.dogbreeds.net.DogBreedsApi

class AllBreedsRepositoryImpl(
    private val dogBreedsApi: DogBreedsApi,
    private val allDogBreedsDataMapper: DataMapper<AllBreedsApi, List<Breed>>
) : AllBreedsRepository {

    override suspend fun retrieveAllDogBreeds(): List<Breed> {
        val launchesApi = dogBreedsApi.fetchAllDogBreeds()
        return allDogBreedsDataMapper.map(launchesApi)
    }
}
