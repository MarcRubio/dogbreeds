package com.cuvva.dogbreeds.net.breedsimages

import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.mapper.DataMapper
import com.cuvva.dogbreeds.net.DogBreedsApi

class BreedsImagesRepositoryImpl(
    private val dogBreedsApi: DogBreedsApi,
    private val allDogBreedsDataMapper: DataMapper<BreedsImagesApi, List<BreedsImages>>
) : BreedsImagesRepository {

    override suspend fun retrieveAllBreedsImages(breed: String): List<BreedsImages> {
        val launchesApi = dogBreedsApi.fetchDogBreedsImages(breed)
        return allDogBreedsDataMapper.map(launchesApi)
    }

    override suspend fun retrieveAllSubBreedsImages(
        breed: String,
        subBreed: String
    ): List<BreedsImages> {
        val launchesApi = dogBreedsApi.fetchDogSubBreedsImages(breed, subBreed)
        return allDogBreedsDataMapper.map(launchesApi)
    }
}
