package com.cuvva.dogbreeds.net.breedsimages

data class BreedsImagesApi(
    val message: List<String>,
    val status: String
)
