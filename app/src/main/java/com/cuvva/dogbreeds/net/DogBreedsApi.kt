package com.cuvva.dogbreeds.net

import com.cuvva.dogbreeds.net.allbreeds.AllBreedsApi
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesApi
import retrofit2.http.GET
import retrofit2.http.Path

interface DogBreedsApi {

    @GET("breeds/list/all")
    suspend fun fetchAllDogBreeds(): AllBreedsApi

    @GET("breed/{breed}/images")
    suspend fun fetchDogBreedsImages(@Path("breed") breed: String): BreedsImagesApi

    @GET("breed/{breed}/{subBreeds}/images")
    suspend fun fetchDogSubBreedsImages(
        @Path("breed") breed: String,
        @Path("subBreeds") subBreed: String
    ): BreedsImagesApi
}
