package com.cuvva.dogbreeds.net.breedsimages

import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.net.Repository

interface BreedsImagesRepository : Repository {
    suspend fun retrieveAllBreedsImages(breed: String): List<BreedsImages>
    suspend fun retrieveAllSubBreedsImages(breed: String, subBreed: String): List<BreedsImages>
}
