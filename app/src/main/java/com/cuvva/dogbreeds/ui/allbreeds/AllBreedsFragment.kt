package com.cuvva.dogbreeds.ui.allbreeds

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.cuvva.dogbreeds.DogBreedsApp
import com.cuvva.dogbreeds.R
import com.cuvva.dogbreeds.databinding.FragmentAllBreedsBinding
import com.cuvva.dogbreeds.entity.Breed
import com.cuvva.dogbreeds.ui.DogBreedsActivity
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class AllBreedsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentAllBreedsBinding
    private lateinit var allBreedsAdapter: AllBreedsAdapter

    private val allBreedsViewModel by viewModels<AllBreedsViewModel>(
        factoryProducer = { viewModelFactory }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = initialiseDataBinding(inflater, container)

    private fun initialiseDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_all_breeds, container, false)
        binding.viewmodel = allBreedsViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        observeViewModel()
        initialiseRecyclerView()
    }

    private fun initialiseRecyclerView() {
        binding.allBreedsRecyclerView.layoutManager = LinearLayoutManager(activity)
        allBreedsAdapter = AllBreedsAdapter(itemClick = itemClick)
        binding.allBreedsRecyclerView.adapter = allBreedsAdapter
    }

    private fun observeViewModel() {
        allBreedsViewModel.allBreedsState.observe(viewLifecycleOwner, ::action)
    }

    private fun action(state: AllBreedsState) {
        when (state) {
            is AllBreedsState.ShowError -> showError()
            is AllBreedsState.ShowAllBreeds -> showPosts(state.breeds)
        }
    }

    private val itemClick = fun(breed: String, subBreed: String?) {
        val directions = AllBreedsFragmentDirections
            .actionNavigationAllBreedsToNavigationBreedsImages(breed, subBreed)
        findNavController().navigate(directions)
    }

    private fun showError() {
        view?.let {
            Snackbar.make(it, R.string.fragment_all_breeds_error, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun showPosts(posts: List<Breed>) {
        allBreedsAdapter.replace(posts)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as? DogBreedsActivity)?.let {
            (it.application as DogBreedsApp).appComponent.inject(this)
        }
    }
}
