package com.cuvva.dogbreeds.ui.allbreeds

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.cuvva.dogbreeds.entity.Breed
import com.cuvva.dogbreeds.databinding.ItemAllBreedsBinding

class AllBreedsAdapter(
    private val itemClick: (String, String?) -> Unit,
    private val items: MutableList<Breed> = mutableListOf()
) : RecyclerView.Adapter<AllBreedsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllBreedsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemAllBreedsBinding.inflate(inflater, parent, false)
        return AllBreedsViewHolder(binding, itemClick)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: AllBreedsViewHolder, position: Int) {
        items[position].also {
            holder.bind(it)
            holder.setClickListener(it)
        }
    }

    @MainThread
    fun replace(breeds: List<Breed>) {
        val difference = DiffUtil.calculateDiff(
            AllBreedsDiffUtil(items, breeds)
        )
        items += breeds
        difference.dispatchUpdatesTo(this)
    }

    private class AllBreedsDiffUtil(
        private val oldList: List<Breed>,
        private val newList: List<Breed>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] === newList[newItemPosition]

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].breed == newList[newItemPosition].breed
    }
}
