package com.cuvva.dogbreeds.ui.breedsimages

import com.cuvva.dogbreeds.entity.BreedsImages

sealed class BreedsImagesState {
    object ShowError : BreedsImagesState()
    class ShowAllBreedsImages(val images: List<BreedsImages>) : BreedsImagesState()
}
