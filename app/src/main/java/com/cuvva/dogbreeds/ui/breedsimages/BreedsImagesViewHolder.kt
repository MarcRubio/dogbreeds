package com.cuvva.dogbreeds.ui.breedsimages

import androidx.recyclerview.widget.RecyclerView
import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.databinding.ItemBreedImagesBinding

class BreedsImagesViewHolder(
    private val binding: ItemBreedImagesBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(breedsImages: BreedsImages) {
        binding.item = breedsImages
    }
}
