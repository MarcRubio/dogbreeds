package com.cuvva.dogbreeds.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.cuvva.dogbreeds.R

class DogBreedsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_breeds)
    }

    override fun onSupportNavigateUp() =
        findNavController(R.id.nav_host_dog_breeds).navigateUp()
}
