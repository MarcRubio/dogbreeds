package com.cuvva.dogbreeds.ui.breedsimages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class BreedsImagesViewModel @Inject constructor(
    private val breedsImagesRepository: BreedsImagesRepository
) : ViewModel() {

    private val _breedsImagesState = MutableLiveData<BreedsImagesState>()
    val breedsImagesState: LiveData<BreedsImagesState> get() = _breedsImagesState

    private val allBreedsExceptionHandler =
        CoroutineExceptionHandler { _, e -> showError(e) }

    fun getBreedsImages(breed: String, subBreed: String?) {
        viewModelScope.launch(allBreedsExceptionHandler) {
            val breedsImages = subBreed?.let { subBreed ->
                    breedsImagesRepository.retrieveAllSubBreedsImages(breed, subBreed)
                } ?: breedsImagesRepository.retrieveAllBreedsImages(breed)

            showAllBreeds(breedsImages)
        }
    }

    private fun showAllBreeds(breeds: List<BreedsImages>) {
        postSate(BreedsImagesState.ShowAllBreedsImages(breeds))
    }

    private fun showError(t: Throwable) {
        postSate(BreedsImagesState.ShowError)
        Timber.e("Error retrieving posts $t")
    }

    private fun postSate(state: BreedsImagesState) {
        _breedsImagesState.value = state
    }
}
