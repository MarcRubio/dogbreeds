package com.cuvva.dogbreeds.ui.allbreeds

import com.cuvva.dogbreeds.entity.Breed

sealed class AllBreedsState {
    object ShowError : AllBreedsState()
    class ShowAllBreeds(val breeds: List<Breed>) : AllBreedsState()
}
