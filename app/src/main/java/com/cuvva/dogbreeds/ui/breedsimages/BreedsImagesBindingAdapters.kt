package com.cuvva.dogbreeds.ui.breedsimages

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("loadImageUrl")
fun ImageView.bindLoadUrlImage(url: String?) {
    Glide.with(context)
        .load(url)
        .placeholder(android.R.drawable.stat_notify_sync)
        .error(android.R.drawable.stat_notify_error)
        .into(this)
}
