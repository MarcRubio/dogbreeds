package com.cuvva.dogbreeds.ui.allbreeds

import androidx.recyclerview.widget.RecyclerView
import com.cuvva.dogbreeds.entity.Breed
import com.cuvva.dogbreeds.databinding.ItemAllBreedsBinding

class AllBreedsViewHolder(
    private val binding: ItemAllBreedsBinding,
    private val itemClick: (String, String?) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(post: Breed) {
        binding.item = post
    }

    fun setClickListener(breed: Breed) {
        binding.itemBreedConstraintLayout.setOnClickListener {
            itemClick(breed.breed, breed.subBreed)
        }
    }
}
