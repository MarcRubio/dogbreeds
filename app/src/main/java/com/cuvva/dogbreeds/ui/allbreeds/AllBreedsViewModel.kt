package com.cuvva.dogbreeds.ui.allbreeds

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cuvva.dogbreeds.entity.Breed
import com.cuvva.dogbreeds.net.allbreeds.AllBreedsRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class AllBreedsViewModel @Inject constructor(
    private val allBreedsRepository: AllBreedsRepository
) : ViewModel() {

    private val _allBreedsState = MutableLiveData<AllBreedsState>()
    val allBreedsState: LiveData<AllBreedsState> get() = _allBreedsState

    private val _progressBarVisibility = MutableLiveData<Boolean>()
    val progressBarVisibility: LiveData<Boolean> get() = _progressBarVisibility

    private val allBreedsExceptionHandler =
        CoroutineExceptionHandler { _, e -> showError(e) }

    init {
        getAllBreeds()
    }

    private fun getAllBreeds() {
        loading(true)
        viewModelScope.launch(allBreedsExceptionHandler) {
            val launches = allBreedsRepository.retrieveAllDogBreeds()
            showAllBreeds(launches)
        }
    }

    private fun loading(visible: Boolean) {
        _progressBarVisibility.value = visible
    }

    private fun showAllBreeds(breeds: List<Breed>) {
        loading(false)
        postAction(AllBreedsState.ShowAllBreeds(breeds))
    }

    private fun showError(t: Throwable) {
        loading(false)
        postAction(AllBreedsState.ShowError)
        Timber.e("Error retrieving posts $t")
    }

    private fun postAction(action: AllBreedsState) {
        _allBreedsState.value = action
    }
}
