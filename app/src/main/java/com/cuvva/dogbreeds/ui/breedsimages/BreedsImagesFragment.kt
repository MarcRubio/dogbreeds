package com.cuvva.dogbreeds.ui.breedsimages

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.cuvva.dogbreeds.DogBreedsApp
import com.cuvva.dogbreeds.R
import com.cuvva.dogbreeds.databinding.FragmentBreedsImagesBinding
import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.ui.DogBreedsActivity
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class BreedsImagesFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val args: BreedsImagesFragmentArgs by navArgs()
    private val breed: String by lazy { args.breed }
    private val subBreed: String? by lazy { args.subBreed }

    private lateinit var binding: FragmentBreedsImagesBinding
    private lateinit var breedsImagesAdapter: BreedsImagesAdapter

    private val breedsImagesViewModel by viewModels<BreedsImagesViewModel>(
        factoryProducer = { viewModelFactory }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = initialiseDataBinding(inflater, container)

    private fun initialiseDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_breeds_images, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    private fun initToolBar() {
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as? AppCompatActivity)?.supportActionBar?.title =
            subBreed?.let { subBreed ->
                "$subBreed $breed"
            } ?: breed
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolBar()
        observeViewModel()
        initialiseRecyclerView()
        breedsImagesViewModel.getBreedsImages(breed, subBreed)
    }

    private fun initialiseRecyclerView() {
        binding.breedsImagesRecyclerView.layoutManager = LinearLayoutManager(activity)
        breedsImagesAdapter = BreedsImagesAdapter()
        binding.breedsImagesRecyclerView.adapter = breedsImagesAdapter
    }

    private fun observeViewModel() {
        breedsImagesViewModel.breedsImagesState.observe(viewLifecycleOwner, ::action)
    }

    private fun action(state: BreedsImagesState) {
        when (state) {
            is BreedsImagesState.ShowAllBreedsImages -> showImages(state.images)
            is BreedsImagesState.ShowError -> showError()
        }
    }

    private fun showImages(images: List<BreedsImages>) {
        breedsImagesAdapter.replace(images)
    }

    private fun showError() {
        view?.let {
            Snackbar.make(it, R.string.fragment_all_breeds_error, Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as? DogBreedsActivity)?.let {
            (it.application as DogBreedsApp).appComponent.inject(this)
        }
    }
}
