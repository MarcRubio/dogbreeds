package com.cuvva.dogbreeds.ui.breedsimages

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.databinding.ItemBreedImagesBinding

class BreedsImagesAdapter(
    private val items: MutableList<BreedsImages> = mutableListOf()
) : RecyclerView.Adapter<BreedsImagesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BreedsImagesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemBreedImagesBinding.inflate(inflater, parent, false)
        return BreedsImagesViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BreedsImagesViewHolder, position: Int) {
        items[position].also(holder::bind)
    }

    @MainThread
    fun replace(images: List<BreedsImages>) {
        val difference = DiffUtil.calculateDiff(
            BreedsImagesDiffUtil(items, images)
        )
        items += images
        difference.dispatchUpdatesTo(this)
    }

    private class BreedsImagesDiffUtil(
        private val oldList: List<BreedsImages>,
        private val newList: List<BreedsImages>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] === newList[newItemPosition]

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].breedImageUrl == newList[newItemPosition].breedImageUrl
    }
}
