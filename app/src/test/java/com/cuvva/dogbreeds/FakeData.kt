package com.cuvva.dogbreeds

import com.cuvva.dogbreeds.entity.Breed
import com.cuvva.dogbreeds.entity.BreedsImages
import com.cuvva.dogbreeds.net.allbreeds.AllBreedsApi
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesApi

object Breeds {
    private val breedsApi = mapOf(
        "breed01" to listOf("subBreed01, subBreed02", "subBreed03"),
        "breed02" to listOf("subBreed01, subBreed02"),
        "breed03" to emptyList()
    )
    val allBreedsApi = AllBreedsApi(breedsApi, "status")
    val breeds = listOf(
        Breed("breed01", "subBreed01"),
        Breed("breed01", "subBreed02"),
        Breed("breed01", "subBreed03"),
        Breed("breed02", "subBreed01"),
        Breed("breed01", "subBreed02"),
        Breed("breed01", null)
    )
}

object BreedsImages {
    private val images = listOf(
        "https://images.dog.ceo/breeds/hound-afghan/n02088094_1003.jpg",
        "https://images.dog.ceo/breeds/hound-afghan/n02088094_1007.jpg",
        "https://images.dog.ceo/breeds/hound-afghan/n02088094_1023.jpg"
    )
    val breedsImagesApi = BreedsImagesApi(images, "status")
    val breedsImages = listOf(
        BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_1003.jpg"),
        BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_1007.jpg"),
        BreedsImages("https://images.dog.ceo/breeds/hound-afghan/n02088094_1023.jpg")
    )
}
