package com.cuvva.dogbreeds.ui.allbreeds

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.filters.SmallTest
import com.cuvva.dogbreeds.Breeds.allBreedsApi
import com.cuvva.dogbreeds.Breeds.breeds
import com.cuvva.dogbreeds.TestCoroutineRule
import com.cuvva.dogbreeds.getOrAwaitValue
import com.cuvva.dogbreeds.mapper.AllBreedsDataMapper
import com.cuvva.dogbreeds.net.DogBreedsApi
import com.cuvva.dogbreeds.net.allbreeds.AllBreedsRepositoryImpl
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
@SmallTest
class AllBreedsViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var testCoroutineRule = TestCoroutineRule()

    private lateinit var allBreedsRepositoryImpl: AllBreedsRepositoryImpl
    private lateinit var allBreedsViewModel: AllBreedsViewModel

    private val dogBreedsApi: DogBreedsApi = mock()
    private val allBreedsDataMapper: AllBreedsDataMapper = mock()
    private val progressBarObserver: Observer<Boolean> = mock()

    @Before
    fun setup() {
        allBreedsRepositoryImpl = AllBreedsRepositoryImpl(dogBreedsApi, allBreedsDataMapper)
    }

    @Test
    fun `when fetch of all dog breeds is successful, state is show all dog breeds`() =
        testCoroutineRule.runBlockingTest {
            callAllBreeds()
            val value = allBreedsViewModel.allBreedsState.getOrAwaitValue()
            assertThat(value is AllBreedsState.ShowAllBreeds, `is`(true))
        }

    @Test
    fun `when fetch of all dog breeds throws error, state is show error`() = runBlockingTest {
        val error = Error()
        whenever(dogBreedsApi.fetchAllDogBreeds()).thenThrow(error)
        initConstructor()
        val value = allBreedsViewModel.allBreedsState.getOrAwaitValue()
        assertThat(value is AllBreedsState.ShowError, `is`(true))
    }

    @Test
    fun `During fetch of all dog breeds and after completion, progressBar state is set to false`() =
        runBlockingTest {
            callAllBreeds()
            // NOTE: Since we are making the call during constructor initialisation, observer
            // doesn't register the change to true
            verify(progressBarObserver).onChanged(false)
        }

    private suspend fun callAllBreeds() {
        whenever(dogBreedsApi.fetchAllDogBreeds()).thenReturn(allBreedsApi)
        whenever(allBreedsDataMapper.map(allBreedsApi)).thenReturn(breeds)
        whenever(allBreedsRepositoryImpl.retrieveAllDogBreeds()).thenReturn(breeds)

        initConstructor()
    }

    private fun initConstructor() {
        allBreedsViewModel = AllBreedsViewModel(allBreedsRepositoryImpl).apply {
            progressBarVisibility.observeForever(progressBarObserver)
        }
    }
}
