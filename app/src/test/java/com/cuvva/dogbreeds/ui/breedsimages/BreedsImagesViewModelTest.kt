package com.cuvva.dogbreeds.ui.breedsimages

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import com.cuvva.dogbreeds.BreedsImages.breedsImages
import com.cuvva.dogbreeds.BreedsImages.breedsImagesApi
import com.cuvva.dogbreeds.TestCoroutineRule
import com.cuvva.dogbreeds.getOrAwaitValue
import com.cuvva.dogbreeds.mapper.BreedsImagesDataMapper
import com.cuvva.dogbreeds.net.DogBreedsApi
import com.cuvva.dogbreeds.net.breedsimages.BreedsImagesRepositoryImpl
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString

@ExperimentalCoroutinesApi
@SmallTest
class BreedsImagesViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var testCoroutineRule = TestCoroutineRule()

    private lateinit var breedsImagesRepositoryImpl: BreedsImagesRepositoryImpl
    private lateinit var breedsImagesViewModel: BreedsImagesViewModel

    private val dogBreedsApi: DogBreedsApi = mock()
    private val breedsImagesDataMapper: BreedsImagesDataMapper = mock()

    @Before
    fun setup() {
        breedsImagesRepositoryImpl =
            BreedsImagesRepositoryImpl(dogBreedsApi, breedsImagesDataMapper)
        breedsImagesViewModel = BreedsImagesViewModel(breedsImagesRepositoryImpl)
    }

    @Test
    fun `when fetch of all dog breeds is successful, state is show all dog breeds`() =
        testCoroutineRule.runBlockingTest {
            whenever(dogBreedsApi.fetchDogBreedsImages(anyString())).thenReturn(breedsImagesApi)
            whenever(breedsImagesDataMapper.map(breedsImagesApi)).thenReturn(breedsImages)
            whenever(breedsImagesRepositoryImpl.retrieveAllBreedsImages(anyString())).thenReturn(
                breedsImages
            )

            breedsImagesViewModel.getBreedsImages(anyString(), null)
            val value = breedsImagesViewModel.breedsImagesState.getOrAwaitValue()
            assertThat(value is BreedsImagesState.ShowAllBreedsImages, `is`(true))
        }

    @Test
    fun `when fetch of all dog breeds throws error, state is show error`() = runBlockingTest {
        val error = Error()
        whenever(dogBreedsApi.fetchDogBreedsImages(anyString())).thenThrow(error)
        breedsImagesViewModel.getBreedsImages(anyString(), null)
        val value = breedsImagesViewModel.breedsImagesState.getOrAwaitValue()
        assertThat(value is BreedsImagesState.ShowError, `is`(true))
    }

    @Test
    fun `when fetch of all dog sub breeds is successful, state is show all dog breeds`() =
        testCoroutineRule.runBlockingTest {
            whenever(dogBreedsApi.fetchDogSubBreedsImages(anyString(), anyString())).thenReturn(breedsImagesApi)
            whenever(breedsImagesDataMapper.map(breedsImagesApi)).thenReturn(breedsImages)
            whenever(breedsImagesRepositoryImpl.retrieveAllSubBreedsImages(anyString(), anyString())).thenReturn(
                breedsImages
            )

            breedsImagesViewModel.getBreedsImages(anyString(), anyString())
            val value = breedsImagesViewModel.breedsImagesState.getOrAwaitValue()
            assertThat(value is BreedsImagesState.ShowAllBreedsImages, `is`(true))
        }

    @Test
    fun `when fetch of all dog sub breeds throws error, state is show error`() = runBlockingTest {
        val error = Error()
        whenever(dogBreedsApi.fetchDogSubBreedsImages(anyString(), anyString())).thenThrow(error)
        breedsImagesViewModel.getBreedsImages(anyString(), anyString())
        val value = breedsImagesViewModel.breedsImagesState.getOrAwaitValue()
        assertThat(value is BreedsImagesState.ShowError, `is`(true))
    }
}
