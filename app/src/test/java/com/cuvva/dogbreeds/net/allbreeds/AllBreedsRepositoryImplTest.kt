package com.cuvva.dogbreeds.net.allbreeds

import androidx.test.filters.SmallTest
import com.cuvva.dogbreeds.Breeds.allBreedsApi
import com.cuvva.dogbreeds.Breeds.breeds
import com.cuvva.dogbreeds.mapper.AllBreedsDataMapper
import com.cuvva.dogbreeds.net.DogBreedsApi
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
@SmallTest
class AllBreedsRepositoryImplTest {
    private lateinit var allBreedsRepository: AllBreedsRepositoryImpl

    private val dogBreedsApi: DogBreedsApi = mock()
    private val allBreedsDataMapper: AllBreedsDataMapper = mock()

    @Before
    fun setUp() {
        allBreedsRepository = AllBreedsRepositoryImpl(dogBreedsApi, allBreedsDataMapper)
    }

    @Test
    fun `when all breeds fetched by api, return list of breed`() = runBlockingTest {
        whenever(dogBreedsApi.fetchAllDogBreeds()).thenReturn(allBreedsApi)
        whenever(allBreedsDataMapper.map(allBreedsApi)).thenReturn(breeds)

        val allBreedsFromRepository = allBreedsRepository.retrieveAllDogBreeds()
        assertThat(allBreedsFromRepository, `is`(breeds))
    }
}
