package com.cuvva.dogbreeds.net.breedsimages

import androidx.test.filters.SmallTest
import com.cuvva.dogbreeds.BreedsImages.breedsImages
import com.cuvva.dogbreeds.BreedsImages.breedsImagesApi
import com.cuvva.dogbreeds.mapper.BreedsImagesDataMapper
import com.cuvva.dogbreeds.net.DogBreedsApi
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString

@ExperimentalCoroutinesApi
@SmallTest
class BreedsImagesRepositoryImplTest {
    private lateinit var breedsImagesRepository: BreedsImagesRepositoryImpl

    private val dogBreedsApi: DogBreedsApi = mock()
    private val breedsImagesDataMapper: BreedsImagesDataMapper = mock()

    @Before
    fun setUp() {
        breedsImagesRepository = BreedsImagesRepositoryImpl(dogBreedsApi, breedsImagesDataMapper)
    }

    @Test
    fun `when breeds images fetched by api, return breed images`() = runBlockingTest {
        whenever(dogBreedsApi.fetchDogBreedsImages(anyString())).thenReturn(breedsImagesApi)
        whenever(breedsImagesDataMapper.map(breedsImagesApi)).thenReturn(breedsImages)

        val breedsImagesFromRepository = breedsImagesRepository.retrieveAllBreedsImages(anyString())
        assertThat(breedsImagesFromRepository, `is`(breedsImages))
    }

    @Test
    fun `when sub breeds images fetched by api, return breed images`() = runBlockingTest {
        whenever(dogBreedsApi.fetchDogSubBreedsImages(anyString(), anyString())).thenReturn(breedsImagesApi)
        whenever(breedsImagesDataMapper.map(breedsImagesApi)).thenReturn(breedsImages)

        val breedsImagesFromRepository = breedsImagesRepository.retrieveAllSubBreedsImages(anyString(), anyString())
        assertThat(breedsImagesFromRepository, `is`(breedsImages))
    }
}
